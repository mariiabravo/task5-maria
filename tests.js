
test("Valid plate",function(assert){

	assert.equal(isValidPlate('4892CPR'),true,"VALID PLATE");
	assert.equal(isValidPlate('1234SNZ'),true,"VALID PLATE");
	assert.equal(isValidPlate('4298YTW'),true,"VALID PLATE");
	assert.equal(isValidPlate('9611TKM'),true,"VALID PLATE");
	assert.equal(isValidPlate('6751LVS'),true,"VALID PLATE");

});

test("No valid plate",function(assert){

	assert.equal(isValidPlate('4892CPA'),false,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('2192EPS'),false,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('9876COA'),false,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('1111CPU'),false,"USE OF VOWELS ON THE LETTER PART");
	assert.equal(isValidPlate('3256SIW'),false,"USE OF VOWELS ON THE LETTER PART");

	assert.equal(isValidPlate('2233SWÑ'),false,"USE OF THE INVALID LETTER Ñ ");
	assert.equal(isValidPlate('7523TQT'),false,"USE OF THE INVALID LETTER Q ");

	//assert.equal(isValidPlate('22333WL'),false,"USE OF MORE DIGITS THAN THE NECESSARY ");
	//assert.equal(isValidPlate('223TSSR'),false,"USE OF MORE LETTERS THAN THE NECCESSARY ");

	assert.equal(isValidPlate('2233SW'),false,"USE OF LESS DIGITS THAN THE NECCESARY");

	//assert.equal(isValidPlate('2675trs'),false,"USE OF LOWERCASE LETTERS");
	//assert.equal(isValidPlate('2675TRs'),false,"USE OF LOWERCASE LETTERS");

	assert.equal(isValidPlate('VBP3456'),false,"WRONG ORDER OF THE CHARACTERS");
	assert.equal(isValidPlate('V34DD56'),false,"WRONG ORDER OF THE CHARACTERS");

	//assert.equal(isValidPlate('123-VBT'),false,"USE OF ADITTIONAL CHARACTERS");
	//assert.equal(isValidPlate('456?RRR'),false,"USE OF ADITTIONAL CHARACTERS");

});